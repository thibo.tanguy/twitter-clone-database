# Twitter clone database

Base de donnée MySql pour stocker les données du projet. La base de données contient des posts, des commentaires, des likes ... liés à des utilisateurs.
## Informations

Le script SQL pour la création de la base de donnée se situe dans le dossier resources.

```
database : twitter_clone
login : root
password : test
```

## Installation

#### 1. Build l'image

```bash
docker build -t twitter-clone-database .
```

#### 2. Lancer le container à partir de l'image

```bash
docker run --name twitter-clone-database -p 3306:3306 -d twitter-clone-database
```
