CREATE DATABASE twitter_clone;
USE twitter_clone;

CREATE TABLE Users
(
    Id       INT PRIMARY KEY AUTO_INCREMENT,
    Name     VARCHAR(150)  NOT NULL,
    Email    VARCHAR(130)  NOT NULL,
    Username VARCHAR(200)  NOT NULL,
    Password VARCHAR(200)  NOT NULL,
    Image    VARCHAR(1500) NULL DEFAULT 'https://drive.google.com/uc?export=view&id=1kWAD-v4gugA_edk33b0O-KAtQZxVUiuF',
    Role     VARCHAR(150)  NULL DEFAULT 'user'
);

CREATE TABLE Roles
(
    Id   INT PRIMARY KEY AUTO_INCREMENT,
    Name VARCHAR(100) NOT NULL
);

CREATE TABLE UserRoles
(
    Id     INT PRIMARY KEY AUTO_INCREMENT,
    UserId INT NOT NULL,
    RoleId INT NOT NULL,
    FOREIGN KEY (UserId) REFERENCES Users (id),
    FOREIGN KEY (RoleId) REFERENCES Roles (id)
);

CREATE TABLE Followings
(
    Id         INT PRIMARY KEY AUTO_INCREMENT,
    FollowerId INT NOT NULL,
    FollowedId INT NOT NULL,
    FOREIGN KEY (FollowerId) REFERENCES Users (Id),
    FOREIGN KEY (FollowedId) REFERENCES Users (Id)
);

CREATE TABLE Posts
(
    Id      INT PRIMARY KEY AUTO_INCREMENT,
    UserId  INT         NOT NULL,
    Title   VARCHAR(40) NOT NULL,
    Content TEXT(400)   NOT NULL,
    Date    VARCHAR(100),
    FOREIGN KEY (UserId) REFERENCES Users (Id)
);

CREATE TABLE PostImages
(
    Id     INT PRIMARY KEY AUTO_INCREMENT,
    UserId INT           NOT NULL,
    PostId INT           NOT NULL,
    Url    VARCHAR(1500) NOT NULL,
    Alt    VARCHAR(500)  NULL,
    FOREIGN KEY (UserId) REFERENCES Users (Id),
    FOREIGN KEY (PostId) REFERENCES Posts (Id)
);

CREATE TABLE Comments
(
    Id      INT PRIMARY KEY AUTO_INCREMENT,
    UserId  INT  NOT NULL,
    PostId  INT  NOT NULL,
    Content TEXT NOT NULL,
    Date    VARCHAR(100),
    FOREIGN KEY (UserId) REFERENCES Users (Id),
    FOREIGN KEY (PostId) REFERENCES Posts (Id)
);

-- Likes on Posts
CREATE TABLE PostLikes
(
    Id     INT PRIMARY KEY AUTO_INCREMENT,
    UserId INT NOT NULL,
    PostId INT NOT NULL,
    FOREIGN KEY (UserId) REFERENCES Users (Id),
    FOREIGN KEY (PostId) REFERENCES Posts (Id)
);

-- Likes on Comments
CREATE TABLE CommentLikes
(
    Id        INT PRIMARY KEY AUTO_INCREMENT,
    UserId    INT NOT NULL,
    CommentId INT NOT NULL,
    FOREIGN KEY (UserId) REFERENCES Users (Id),
    FOREIGN KEY (CommentId) REFERENCES Comments (Id)
);

CREATE TABLE Conversations
(
    Id          INT PRIMARY KEY AUTO_INCREMENT,
    SenderId    INT NOT NULL,
    RecipientId INT NOT NULL,
    CreatedAt   DATETIME,
    FOREIGN KEY (SenderId) REFERENCES Users (Id),
    FOREIGN KEY (RecipientId) REFERENCES Users (Id)
);

CREATE TABLE Messages
(
    Id             INT PRIMARY KEY AUTO_INCREMENT,
    ConversationId INT  NOT NULL,
    SenderId       INT  NOT NULL,
    RecipientId    INT  NOT NULL,
    Content        TEXT NOT NULL,
    Date           DATETIME,
    FOREIGN KEY (ConversationId) REFERENCES Conversations (Id),
    FOREIGN KEY (SenderId) REFERENCES Users (Id),
    FOREIGN KEY (RecipientId) REFERENCES Users (Id)
);

CREATE TABLE UserConversations
(
    Id             INT PRIMARY KEY AUTO_INCREMENT,
    UserId         INT NOT NULL,
    ConversationId INT NOT NULL,
    FOREIGN KEY (UserId) REFERENCES Users (id),
    FOREIGN KEY (ConversationId) REFERENCES Conversations (id)
);

-- Indexs
CREATE INDEX idx_UserId_UserRoles ON UserRoles (UserId);
CREATE INDEX idx_RoleId_UserRoles ON UserRoles (RoleId);
CREATE INDEX idx_UserId_Posts ON Posts (UserId);
CREATE INDEX idx_PostId_PostImages ON PostImages (PostId);
CREATE INDEX idx_UserId_PostImages ON PostImages (UserId);
CREATE INDEX idx_UserId_Comments ON Comments (UserId);
CREATE INDEX idx_PostId_Comments ON Comments (PostId);
CREATE INDEX idx_UserId_PostLikes ON PostLikes (UserId);
CREATE INDEX idx_PostId_PostLikes ON PostLikes (PostId);
CREATE INDEX idx_UserId_CommentLikes ON CommentLikes (UserId);
CREATE INDEX idx_CommentId_CommentLikes ON CommentLikes (CommentId);
CREATE INDEX idx_UserId_UserConversations ON UserConversations (UserId);
CREATE INDEX idx_ConversationId_UserConversations ON UserConversations (ConversationId);
CREATE INDEX idx_ConversationId_Messages ON Messages (ConversationId);
CREATE INDEX idx_SenderId_Messages ON Messages (SenderId);
CREATE INDEX idx_RecipientId_Messages ON Messages (RecipientId);

-- Data inserts for development
INSERT INTO Users (Email, Name, Username, Password, Image)
VALUES ('batman@batmail.com', 'Batman', 'batman15', 'batword',
        'https://drive.google.com/uc?export=view&id=1v76EY-FiUKUw0C7JRuu-GYd7q9Ysq7ws'),
       ('superman@supermail.com', 'Superman', 'superman45', 'superword',
        'https://drive.google.com/uc?export=view&id=1Hlt2fm9dCAWPQqMzxuRm-0DZUgWUl3wz'),
       ('wolverine@wolvermail.com', 'Wolverine', 'wolverine214', 'wolverword',
        'https://drive.google.com/uc?export=view&id=17iS1eFU09u5jZGuGGqxeqWh_rwaDkgNI'),
       ('superman@spidermail.com', 'Spiderman', 'spiderman120', 'spiderword',
        'https://drive.google.com/uc?export=view&id=1DPviGN0Oh2Qw98CklBmYeElqdzx0WbYc'),
       ('hulk@hulkmail.com', 'Hulk', 'hulk41', 'hulkword',
        'https://drive.google.com/uc?export=view&id=1qYwy-GyXGGqiFnazrCDtldvf5kxnyIJN'),
       ('wonder-woman@wondermail.com', 'Wonder Woman', 'wonder65', 'wonderword',
        'https://drive.google.com/uc?export=view&id=1NJfh8JKDVJKEIH57CKCXUaAS7ijKMDA3'),
       ('ironman@ironmail.com', 'Ironman', 'ironman12', 'ironword',
        'https://drive.google.com/uc?export=view&id=1JbEhKBKMfabQnD-BH2k00ADU-nGwHKSi');

INSERT INTO Roles (name)
VALUES ('ROLE_USER'),
       ('ROLE_MODERATOR'),
       ('ROLE_ADMIN');

INSERT INTO UserRoles(UserId, RoleId)
VALUES (1, 1),
       (2, 1),
       (3, 1),
       (4, 1),
       (5, 1),
       (6, 1),
       (7, 1);

-- Posts
INSERT INTO Posts (UserId, Title, Content, Date)
VALUES (1, 'Mon premier post',
        'Salut les amis, c''est Batman ici ! Juste pour vous rappeler que même sans superpouvoirs, je suis le meilleur héros de tous. Quelqu''un d''autre a besoin d''un Bat-Signal pour être remarqué ? #SansSuperpouvoirs #BatmanRocks',
        '2023-08-14'),
       (2, 'Des nouvelles passionnantes',
        'Superman ici ! J''ai encore sauvé le monde aujourd''hui, mais je ne veux pas vous ennuyer avec les détails. Quoi de neuf dans le monde des humains ? #SuperNews #ManOfSteel',
        '2023-06-28'),
       (3, 'Salut, le monde',
        'Wolverine ici, et je me demande pourquoi personne n''aime les griffes rétractables autant que moi. C''est un mystère pour moi. #AdmantiumClaws #BestThereIs',
        '2023-01-13'),
       (4, 'Une journée à la plage',
        'Salut tout le monde, c''était génial de se balader sur les plages de New York aujourd''hui. Le sable, le soleil, et les toiles d''araignée, que demander de plus ? #SpiderDay #BeachMode',
        '2023-06-11'),
       (5, 'Aventures culinaires',
        'Salut, les amis ! Aujourd''hui, j''ai mangé une montagne de burritos géants. J''ai un appétit géant pour la nourriture délicieuse. #HulkHungry #BurritoBinge',
        '2023-03-26'),
       (6, 'Mises à jour technologiques',
        'Hello, tout le monde ! Je suis en train de tester un nouveau gadget Amazonien. Il est temps que le monde découvre notre technologie avancée. #AmazonTech #WonderGadgets',
        '2023-06-05'),
       (7, 'Ma dernière invention',
        'Salut à tous ! J''ai créé une nouvelle armure géniale. Si vous voulez en savoir plus sur la technologie Stark, demandez simplement. #StarkTech #Innovation',
        '2023-07-29');

-- Commentaires
INSERT INTO Comments (PostId, UserId, Content, Date)
VALUES (1, 2, 'Hey Batman, tu utilises une lampe de poche pour attirer l''attention ? #BatSignal #Lame', '2023-11-06'),
       (1, 3,
        'Haha, Batman, tu sais que tu as besoin d''un peu de l''aide de l''Amazone pour être à la hauteur. #GirlPower #WonderWomanRules',
        '2023-11-06'),
       (2, 1, 'Tell us the news, Superman!', '2023-11-06'),
       (3, 4,
        'Wolverine, les griffes rétractables sont mignonnes, mais je préfère écraser les choses de mes mains géantes. #HulkSmash #BiggerIsBetter',
        '2023-11-06'),
       (4, 5,
        'Hey Superman, t''as utilisé tes super oreilles pour écouter les rumeurs de la ville ? #SuperEspion #SpiderSenses',
        '2023-11-06'),
       (5, 6,
        'Spiderman, tu as trouvé un trésor enfoui dans le sable ? J''ai besoin d''un nouvel ajout pour ma collection. #IronCollector #TreasureHunt',
        '2023-11-06'),
       (6, 7,
        'Ironman, tu as une belle armure, mais moi, je n''ai besoin d''aucune armure. Hulk est indestructible ! #NoArmorNeeded #HulkStrong',
        '2023-11-06');

INSERT INTO PostLikes (PostId, UserId)
VALUES (1, 4),
       (1, 6),
       (2, 5),
       (3, 2),
       (4, 7),
       (5, 1),
       (6, 3);

INSERT INTO CommentLikes (CommentId, UserId)
VALUES (1, 6),
       (6, 1),
       (2, 7),
       (2, 5),
       (4, 3),
       (4, 2),
       (5, 1);

INSERT INTO PostImages (UserId, PostId, Url, Alt)
VALUES (1, 1,
        'https://static1.cbrimages.com/wordpress/wp-content/uploads/2017/10/spider-man-memes.jpg?q=50&fit=contain&w=1140&h=&dpr=1.5',
        'post'),
       (2, 2,
        'https://i0.wp.com/www.superpouvoir.com/wp-content/uploads/2019/04/my_parents_are_deeaaaaaad-1.jpg?resize=401%2C389&ssl=1',
        'post'),
       (3, 3,
        'https://static1.cbrimages.com/wordpress/wp-content/uploads/2017/10/Wolverine-Meme-Featured.jpg?q=50&fit=contain&w=1140&h=&dpr=1.5',
        'post'),
       (4, 4, 'https://i.pinimg.com/564x/72/46/85/7246859c6daa736128026c6c22e44f7c.jpg', 'post'),
       (5, 5,
        'https://static1.cbrimages.com/wordpress/wp-content/uploads/2017/11/wonder-woman-memes.jpg?q=50&fit=contain&w=1140&h=&dpr=1.5',
        'post'),
       (6, 6,
        'https://www.google.com/search?q=hulk+meme&rlz=1C1VDKB_frFR973FR973&oq=hulk+meme&gs_lcrp=EgZjaHJvbWUyDggAEEUYFBg5GIcCGIAEMgcIARAAGIAEMgcIAhAAGIAEMggIAxAAGBYYHjIICAQQABgWGB4yCAgFEAAYFhgeMggIBhAAGBYYHjIICAcQABgWGB4yCAgIEAAYFhgeMggICRAAGBYYHtIBCDE0NzBqMGo0qAIAsAIA&sourceid=chrome&ie=UTF-8#vhid=8QpFzW_qfP6m0M&vssid=l',
        'post'),
       (7, 7, 'https://i.pinimg.com/564x/fb/4a/fb/fb4afb5ea3720dec002688e536ff1657.jpg', 'post');

-- Insertion de données dans la table "conversations"
INSERT INTO Conversations (SenderId, RecipientId, CreatedAt)
VALUES (1, 2, '2023-05-23 10:00:00'),
       (2, 3, '2023-05-23 10:00:00'),
       (3, 4, '2023-05-23 10:00:00'),
       (4, 5, '2023-05-23 10:00:00'),
       (6, 7, '2023-05-23 10:00:00'),
       (1, 3, '2023-05-23 10:00:00'),
       (1, 4, '2023-05-23 10:00:00'),
       (1, 5, '2023-05-23 10:00:00'),
       (4, 1, '2023-05-23 10:00:00'),
       (6, 1, '2023-05-23 10:00:00'),
       (7, 3, '2023-05-23 10:00:00');

-- Insertion de données dans la table "messages"
INSERT INTO Messages (ConversationId, SenderId, RecipientId, Content, Date)
VALUES (1, 1, 2, 'Message 1 de John à Jane', '2023-05-23 10:00:00'),
       (2, 2, 3, 'Message 2 de Jane à Alice', '2023-05-24 11:00:00'),
       (3, 3, 4, 'Message 3 de Alice à Bob', '2023-05-25 12:00:00'),
       (4, 4, 5, 'Message 4 de Bob à Emma', '2023-05-26 13:00:00'),
       (5, 5, 6, 'Message 5 de Emma à Michael', '2023-05-27 14:00:00'),
       (6, 6, 7, 'Message 6 de Michael à Laura', '2023-05-28 15:00:00'),
       (7, 3, 2, 'Message 7 de Laura à David', '2023-05-29 16:00:00'),
       (1, 4, 1, 'Message 8 de David à Sophie', '2023-05-30 17:00:00'),
       (2, 3, 5, 'Message 9 de Sophie à Mark', '2023-06-01 18:00:00'),
       (3, 1, 6, 'Message 10 de Mark à Emily', '2023-06-02 19:00:00'),
       (4, 5, 1, 'Message 11 de Emily à Andrew', '2023-06-03 20:00:00'),
       (5, 4, 1, 'Message 12 d''Andrew à John', '2023-06-04 21:00:00');

-- Insertion de données dans la table "user_conversations"
INSERT INTO UserConversations (UserId, ConversationId)
VALUES (1, 1),
       (2, 2),
       (3, 3),
       (4, 4),
       (5, 5),
       (6, 6),
       (7, 7);

INSERT INTO Followings (FollowerId, FollowedId)
VALUES (1, 2),
       (2, 3),
       (3, 4),
       (4, 5),
       (5, 6),
       (6, 7),
       (7, 1),
       (1, 6),
       (1, 5),
       (4, 1),
       (3, 1),
       (1, 2);
